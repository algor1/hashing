/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.hashing;

/**
 *
 * @author ASUS
 */
public class testHashing {
    public static void main(String[] args) {
        Hashing hs = new Hashing();
        hs.put(15, "Narachai");
        hs.put(17, "Praphaisan");
        hs.put(1, "ASDF");
        
        System.out.println(hs.get(7));
        for(int i = 0 ; i < 10 ; i ++){
            System.out.println(hs.get(i)); //ลองโชว์ทั้งหมด
        }
        System.out.println("--------------------");
        hs.put(25, "Narachai2");
        hs.put(35, "Narachai3");
        hs.put(45, "Narachai4");
        hs.put(55, "Narachai5");
        hs.put(65, "Narachai6");
        hs.remove(17);
        for(int i = 0 ; i < 10 ; i ++){
            System.out.println(hs.get(i)); //ลองโชว์ทั้งหมด
        }
    }
}
